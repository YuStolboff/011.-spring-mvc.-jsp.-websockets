package com.epam.kb.spring.mvc.service;

import com.epam.kb.spring.mvc.model.User;

public interface UserService {
    void addUser(User user);

    public User getUser();

    public void setUser(User user);

}
