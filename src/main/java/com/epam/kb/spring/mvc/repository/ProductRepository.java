package com.epam.kb.spring.mvc.repository;

import com.epam.kb.spring.mvc.model.Catalog;
import com.epam.kb.spring.mvc.model.Good;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {

    private static final Logger log = LoggerFactory.getLogger(ProductRepository.class);

    private final static String path = "src/main/webapp/resources/data/goods.json";
    private final List<Good> goodList = getJson(path);
    private final String stringJSON = getStringJson(path);

    private List<Good> getJson(String path) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            Catalog catalog = objectMapper.readValue(new FileInputStream(path), Catalog.class);
            return new ArrayList<>(catalog.getGoods());
        } catch (IOException e) {
            log.info("IOException {}", e);
        }
        return null;
    }

    private String getStringJson(String path) {
        try (BufferedReader stringJson = new BufferedReader (new FileReader(path))){

            StringBuilder stringBuilder = new StringBuilder();

            String s;
            while((s = stringJson.readLine())!= null){

                stringBuilder.append(s);
            }

            return String.valueOf(stringBuilder);
        } catch (IOException e) {
            log.info("IOException {}", e);
        }
        return "";

    }

    private String generatesCatalog(List<Good> goodList, String category) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Good good : goodList) {
            if (good.getCat().equals(category)) {
                stringBuilder = stringBuilder.append("<br>").append("<div class=\"products\" id=\"")
                        .append(good.getId()).append("\">").append("<div class=\"img\"><img src=\"")
                        .append(good.getImg()).append("\"></img><p>").append(good.getName()).append("<br>")
                        .append("</p></div><div class=\"text_product\">$ ").append(good.getPrice())
                        .append("</div><div><button class=\"button\" id=\"").append(good.getId())
                        .append("\">В корзину</button></div></div>");
            }
        }
        return String.valueOf(stringBuilder);
    }

    public List<Good> getGoodList() {
        return goodList;
    }

    public String getStringJSON() {
        return stringJSON;
    }
}
