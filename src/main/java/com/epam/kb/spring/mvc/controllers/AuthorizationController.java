package com.epam.kb.spring.mvc.controllers;

import com.epam.kb.spring.mvc.model.Good;
import com.epam.kb.spring.mvc.model.Product;
import com.epam.kb.spring.mvc.model.User;
import com.epam.kb.spring.mvc.repository.ProductRepository;
import com.epam.kb.spring.mvc.repository.UserRepository;
import com.epam.kb.spring.mvc.service.UserManager;
import com.epam.kb.spring.mvc.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;

@Controller
public class AuthorizationController {


    private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    UserService userManager;
    private List<String> selectedGoods = new ArrayList<>();


    @GetMapping("authorization")
    public ModelAndView main2() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("authorization");
        return modelAndView;
    }

    @GetMapping("index")
    public ModelAndView index(@ModelAttribute ProductRepository productRepository) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");

        List<Good> goods = productRepository.getGoodList();

        modelAndView.addObject("user", userManager.getUser());
        modelAndView.addObject("goods", goods);
        return modelAndView;
    }

    @GetMapping("basket")
    public ModelAndView basket(@ModelAttribute ProductRepository productRepository) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("basket");

        List<Good> goods = new ArrayList<>();
        for (Good good : productRepository.getGoodList()) {
            if (selectedGoods.contains(good.getId())) {
                goods.add(good);
            }
        }
        modelAndView.addObject("selectedGoods", selectedGoods);
        modelAndView.addObject("user", userManager.getUser());
        modelAndView.addObject("goods", goods);

        return modelAndView;
    }

    @GetMapping("/")
    public ModelAndView getRootPage(@ModelAttribute @Valid User user, BindingResult result) {

        ModelAndView modelAndView = new ModelAndView("redirect:/authorization");
        if (result.hasErrors()) {
            modelAndView.setViewName("authorization");
            return modelAndView;
        }
        //return "/authorization";
        return modelAndView;
    }

    @PostMapping("/index")
    public ModelAndView authorization(@ModelAttribute @Valid User user, BindingResult result,
            @ModelAttribute UserRepository userRepository, @ModelAttribute ProductRepository productRepository) {

        ModelAndView modelAndView = new ModelAndView("redirect:/index");
        if (result.hasErrors()) {
            modelAndView.setViewName("authorization");
            return modelAndView;
        }
        user.setId(userRepository.getMaxUserId());

        userManager.setUser(user);


        List<String> logins = new ArrayList<>();
        for (int i = 0; i < userRepository.getUserList().size(); i++) {
            log.info("USER, {}", userRepository.getUserList().get(i).getLogin());
            logins.add(userRepository.getUserList().get(i).getLogin());
        }
        if (!logins.contains(user.getLogin())) {
            userRepository.setUserList(user);
        }


        modelAndView.addObject("user", user);
        modelAndView.setViewName("index");

        log.info("user, {}", user);
        log.info("map, {}", userRepository.getUserList());
        log.info("userManagerA, {}", userManager.getUser().getLogin());

        List<Good> goods = productRepository.getGoodList();
        String products = productRepository.getStringJSON();

        userManager.setUser(user);

        modelAndView.addObject("user", userManager.getUser());
        modelAndView.addObject("goods", goods);
        modelAndView.addObject("products", products);
        modelAndView.setViewName("index");

        log.info("mainUser, {}", userManager.getUser());
        log.info("mainUser, {}", userRepository.getUserList());

        return modelAndView;
    }

    @RequestMapping("/search")
    public ModelAndView clickSearch(@ModelAttribute ProductRepository productRepository,
            @RequestParam("quest") String quest) {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("search");

        List<Good> goods = new ArrayList<>();
        for (Good good : productRepository.getGoodList()) {
            if (good.getName().contains(quest)) {
                goods.add(good);
            }
        }
        modelAndView.addObject("goods", goods);
        modelAndView.addObject("user", userManager.getUser());

        log.info("quest, {}", quest);
        return modelAndView;
    }

    @RequestMapping("/basket")
    public ModelAndView clickBasket(@ModelAttribute ProductRepository productRepository,
            @RequestParam("basket") String value) {
        ModelAndView modelAndView = new ModelAndView("forward:/index");
        modelAndView.setViewName("basket");


        selectedGoods.add(value);

        List<Good> goods = new ArrayList<>();

        double discount = userManager.getUser().getDiscount();
        double amount = 0;
        double total;
        for (Good good : productRepository.getGoodList()) {
            if (selectedGoods.contains(good.getId())) {
                goods.add(good);
                amount = amount + Integer.parseInt(good.getPrice());
            }
        }
        total = amount - amount * (discount / 100);


        modelAndView.addObject("selectedGoods", selectedGoods);
        modelAndView.addObject("user", userManager.getUser());
        modelAndView.addObject("goods", goods);
        modelAndView.addObject("selectedGoods", selectedGoods);
        modelAndView.addObject("amount", amount);
        modelAndView.addObject("discount", discount);
        modelAndView.addObject("total", total);

        log.info("selectedGoods, {}", selectedGoods);
        log.info("mainUser, {}", userManager.getUser());
        log.info("discount, {}", discount);
        log.info("amount, {}", amount);
        log.info("total, {}", total);
        return modelAndView;
    }
}
