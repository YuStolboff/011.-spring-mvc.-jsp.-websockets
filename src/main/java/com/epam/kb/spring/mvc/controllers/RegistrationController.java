package com.epam.kb.spring.mvc.controllers;

import com.epam.kb.spring.mvc.model.User;
import com.epam.kb.spring.mvc.repository.UserRepository;
import com.epam.kb.spring.mvc.service.UserManager;
import com.epam.kb.spring.mvc.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class RegistrationController {

    private static final Logger log = LoggerFactory.getLogger(RegistrationController.class);

    @Autowired
    UserService userManager;

//    @GetMapping("registration")
//    public String getRegistration(Model model) {
//        return "registration";
//    }


    @GetMapping("registration")
    public ModelAndView main3() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", new User());
        modelAndView.setViewName("registration");
        return modelAndView;
    }


    @PostMapping("/reg")
    public ModelAndView registration(@ModelAttribute User user, BindingResult result,
            @ModelAttribute UserRepository userRepository, ModelMap modelMap) {
        ModelAndView modelAndView = new ModelAndView("redirect:/index");

        if (result.hasErrors()) {
            modelAndView.setViewName("registration");
            return modelAndView;
        }


        user.setId(userRepository.getMaxUserId());

        userRepository.setUserList(user);
        userManager.setUser(user);

        modelAndView.addObject("user", user);

        //log.info("user, {}", user);
        log.info("useruserManager, {}", userManager.getUser());
        log.info("map, {}", userRepository.getUserList());

        return modelAndView;
    }
}
