package com.epam.kb.spring.mvc.repository;

import com.epam.kb.spring.mvc.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class UserRepository {

    private final static List<User> userList = new ArrayList<>();

    static {
        userList.add(new User(0L, "Petrov", "petr"));
        userList.add(new User(1L, "Ivanov", "ivan"));
    }

    public Long getMaxUserId() {
        long max = 0;
        for (User user : userList) {
            if (user.getId() > max) {
                max = user.getId() + 1;
            }
        }
        return max;
    }

    public List< User> getUserList() {
        return userList;
    }

    public void setUserList(User user) {
        userList.add(user);
    }
    //    public User createAppUser() {
//        Long userId = this.getMaxUserId() + 1;
//                User user = new User(userId, user.getFirstName(), form.getLastName(), user.getPass(), user.getEmail());
//
//        userMap.put(userId, user);
//        return user;
//    }


    @Override
    public String toString() {
        return "UserRepository{" +
                "userMap=" + userList +
                '}';
    }
}
