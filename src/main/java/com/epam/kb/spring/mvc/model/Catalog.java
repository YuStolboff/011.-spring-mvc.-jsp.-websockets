package com.epam.kb.spring.mvc.model;

import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "goods"
})
public class Catalog {

    @JsonProperty("goods")
    private List<Good> goods = new ArrayList<>();

    @JsonProperty("goods")
    public List<Good> getGoods() {
        return goods;
    }
}
