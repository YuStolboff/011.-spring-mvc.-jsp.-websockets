package com.epam.kb.spring.mvc.model.enums;

public enum UserRole {

    ADMIN,
    CUSTOMER;

    UserRole() {
    }
}
