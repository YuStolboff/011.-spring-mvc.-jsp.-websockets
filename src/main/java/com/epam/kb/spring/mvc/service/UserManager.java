package com.epam.kb.spring.mvc.service;

import com.epam.kb.spring.mvc.model.User;
import com.epam.kb.spring.mvc.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Random;

@Service
@Scope(value = "session", proxyMode = ScopedProxyMode.INTERFACES)
public class UserManager implements UserService {




    @Autowired
    private User user;

    @Qualifier
    private UserRepository userRepository;

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void addUser(User user) {



    }
}
