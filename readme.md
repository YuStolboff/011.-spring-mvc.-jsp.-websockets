#Practice task

1. Web Basics practise converts to Spring MVC.
2. HTML templates convert to JSP templates (Spring Tag Library, form:form).
3. Implement login form using HttpSession (session scope).
4. Create REST-service for main objects, for example list of products. Controllers should return the test data

    * Implement CRUD operations (Create product, Read product, Update product ((star)), Delete product)
    * Implement a search objects by attributes, for example: https://dev.twitter.com/rest/reference/get/search/tweets) (for example, byName, byPrice)
    * The application must be thread-safe
    * Implement paging as  https://dev.twitter.com/rest/public/timelines, for example: https://dev.twitter.com/rest/reference/get/lists/subscribers) ((star))
  
5. Use both type of requests (full page refresh and ajax)
6. Run app using Spring Boot.